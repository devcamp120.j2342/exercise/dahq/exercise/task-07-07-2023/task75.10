-- 1 Hiển thị toàn bộ thông tin của tất cả sinh viên
SELECT * FROM `students`;
-- 2 Hiển thị mã sinh viên và username của tất cả sinh viên
SELECT students.student_code,students.user_name FROM students;
-- 3 Hiển thị toàn bộ thông tin môn học, mà có credit (số tin chỉ) > 3
SELECT *FROM subjects WHERE subjects.credit>3;
-- 4 Hiển thị toàn bộ thông tin điểm có ngày thi (exam_date) trước ngày 01/05/2021
SELECT * FROM grades WHERE grades.exam_date<"2021-05-01";
-- 5 Hiển thị thông tin bảng điểm có các thông tin: student_code, subject_id, grade và exam_date 
SELECT students.student_code,grades.subject_id,grades.grade,grades.exam_date
    FROM grades INNER JOIN students ON grades.student_id=students.id;
-- 6 Hiển thị thông tin bảng điểm có các thông tin: student_code, fullname, subject_id, grade và exam_date của môn có subject_id = 1
SELECT students.student_code,CONCAT_WS (" ",students.first_name, students.last_name) 
    AS full_name,grades.subject_id,grades.grade,grades.exam_date 
    FROM grades INNER JOIN students ON grades.student_id=students.id;
-- 7 Hiển thị toàn bộ thông tin bảng điểm: Tên môn, mã sinh viên, họ và tên, điểm, ngày thi
SELECT students.student_code,CONCAT_WS (" ",students.first_name, students.last_name) 
    AS full_name,grades.grade,grades.exam_date,subjects.subject_name 
    FROM grades INNER JOIN students ON grades.student_id=students.id 
    INNER JOIN subjects ON grades.subject_id=subjects.id;
-- 8 Tổng hợp xem mỗi môn có bao nhiêu học viên đã thi: lấy ra subject_id và số lượng tương ứng
SELECT COUNT(grades.student_id) AS student_num ,grades.subject_id 
    FROM grades GROUP BY grades.subject_id;
-- 9 Tổng hợp ra các môn có số lượng học viên đã thi > 5. Lấy ra subject_id và số lượng tương ứng
SELECT COUNT(grades.student_id) AS student_num ,grades.subject_id 
    FROM grades GROUP BY grades.subject_id HAVING student_num>5;
-- 10 Tổng hợp ra các môn có số lượng học viên đã thi > 5. Lấy ra tên môn và số lượng tương ứng
SELECT COUNT(grades.student_id) AS student_num ,subjects.subject_name 
    FROM grades INNER JOIN subjects ON grades.subject_id=subjects.id 
    GROUP BY grades.subject_id HAVING student_num>5;
-- 11 Lấy ra thông tin 3 điểm thi cao nhất: môn học, tên sinh viên, điểm thi, ngày thi
SELECT CONCAT_WS(students.first_name,students.last_name) 
    AS full_name,subjects.subject_name,grades.grade,grades.exam_date 
    FROM grades INNER JOIN subjects ON grades.subject_id=subjects.id 
    INNER JOIN students ON grades.student_id=students.id ORDER BY grades.grade DESC LIMIT 0,3;
